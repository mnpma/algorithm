package com.hm;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by W8 on 16.01.2018.
 */
public class MergeAppTest {
    private MergeApp mergeApp = new MergeApp();

    @Test
    public void testMerge() {
        int[] array1={5,6};
        int[] array2={2,3,4};
        int[] r = mergeApp.merge(array1, array2);
        int[] expected={2,3,4,5,6};
        for(int i = 0; i < r.length ; i++) {
            assertTrue(r[i]==expected[i]);
        }
    }
}
