package com.hm;

import java.util.Arrays;

/**
 * Created by W8 on 16.01.2018.
 */
public class MergeApp {
    public static void main(String[] args) {
        MergeApp app = new MergeApp();
        int[] array1={5,6,7,8,13,14,15};
        int[] array2={2,3,4,9,11};
        int[] r = app.merge(array1, array2);
        System.out.println(Arrays.toString(r));
    }

    public int[] merge(int[] array1, int[] array2) {
        int[] result = new int[array1.length+array2.length];

        for(int r=0, arr1=0, arr2=0;r<array1.length+array2.length || arr1 < array1.length || arr2 < array2.length;) {
            if(arr1 < array1.length
                    && arr2 < array2.length) {
                result[r++] = array1[arr1] < array2[arr2] ? array1[arr1++] : array2[arr2++];
            } else if(arr1 < array1.length) {
                result[r++] = array1[arr1++];
            } else if(arr2 < array2.length) {
                result[r++] = array2[arr2];
            }
        }
        return result;
    }
}
