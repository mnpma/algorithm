package com.hm;

/**
 * Created by W8 on 12.12.2017.
 */
public class SearchSingleDigit {
    public static void main(String[] args){
        SearchSingleDigit searchSingleDigit = new SearchSingleDigit();
        int[] digits = searchSingleDigit.getInputData();
        int digit = searchSingleDigit.calculateSingleDigit(digits);
        System.out.println(String.format("single Digit is %d", digit));

    }
    public int[] getInputData() {
        int[] digits = {1, 1, 3, 2, 2, 4, 4, 9, 9};
        return digits;
    }

    public int calculateSingleDigit(int[] digits){
        if(digits == null || digits.length == 0) {
            return -1;
        }
        int result = digits[0];
        int i = 1;
        while(i < digits.length){
            result ^= digits[i++];
        }
        return result;
    }
}
