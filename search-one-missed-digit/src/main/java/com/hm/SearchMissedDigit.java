package com.hm;

/**
 * Created by W8 on 12.12.2017.
 */
public class SearchMissedDigit {
    public static void main(String[] args) {
        SearchMissedDigit searchMissedDigit = new SearchMissedDigit();
        int[] digits = searchMissedDigit.getInputData();
        int missedDigit = searchMissedDigit.calculateMissedDigit(digits);
        System.out.println(String.format("missedDigit is %d", missedDigit));
    }

    public int[] getInputData() {
        int[] digits = {1, 6, 3, 2, 4, 7, 8, 9};
        return digits;
    }

    public int calculateMissedDigit(int[] digits) {
        //formula of Arithmetic progression is n*(n+1)/2
        int summa = (digits.length + 1) * (digits.length + 2) / 2;
        int s = 0;
        for (int i : digits) {
            s += i;
        }
        return summa - s;
    }
}
