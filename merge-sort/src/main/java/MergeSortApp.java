/**
 * @author mpimenova
 */
public class MergeSortApp<T extends Comparable> {
    public static void main(String... args) {
        System.out.println("Start..");
        //open and read files
        String inFile = System.getProperty("file.in");
        String outFile = System.getProperty("file.out");
         Integer[] a = new Integer[5];
        a[0] = 3;
        a[1] = 4;
        a[2] =1;
        a[3] = 6;
        a[4] = 0;
        MergeSortApp<Integer> mergeSortApp = new MergeSortApp<Integer>();
        mergeSortApp.mergeSort(a, 0, 4);
        for(Integer el: a ) {
            System.out.println(el);
        }
    }

    /**
     * Merge sort
     * @param a sorted array
     * @param lb left boundary
     * @param ub right boundary
     */
    void mergeSort(T a[], int lb, int ub) {
        int split;
        if(lb < ub) {
            split = (lb + ub) / 2;
            mergeSort(a, lb, split);
            mergeSort(a, split + 1, a.length-1);
            merge(a, lb, split, ub);
        }
    }

    void merge(T a[], int lb, int split, int ub) {
        //merge sorted array's part into buffer temp
        // and then move  temp into a[lb]...a[ub]

        //current position
        int  pos1 = lb;
        //current position from the second array
        int pos2 = split + 1;
        //current position record to temp
        int pos3=0;

        Object[] temp = new Object[ub-lb + 1];
        //merge goes until there exists one element in each sequences
        while(pos1 <= split && pos2 <= ub) {
            if(a[pos1].compareTo( a[pos2]) < 0){
                temp[pos3++] = a[pos1++];
            } else {
                temp[pos3++] = a[pos2++];
            }
        }

        while(pos2 <= ub) {
            temp[pos3++] = a[pos2++];
        }
        while(pos1 <= split) {
            temp[pos3++] = a[pos1++];

        }
        for(pos3 = 0; pos3 < ub-lb+1; pos3++) {
            a[lb+pos3] = (T)temp[pos3];
        }
    }
}

