package com.hm.impl;

import com.hm.Cmp;

/**
 * @author mpimenova
 */
public class Icmp implements Cmp {
    @Override
    public int cmp(Object x, Object y) {
        if(!(x instanceof Integer) || !(y instanceof Integer)){
            throw new IllegalArgumentException();
        }
        return ((Integer)x).compareTo((Integer)y);
    }
}
