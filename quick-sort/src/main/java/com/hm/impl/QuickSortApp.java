package com.hm.impl;

import com.hm.Cmp;

import java.io.*;
import java.util.ArrayList;

/**
 * @author mpimenova
 */
public class QuickSortApp {
    public static void main(String... args) throws IOException {
        System.out.println("Start..");
        InputStream inputFile = QuickSortApp.class.getResourceAsStream("/input.txt");
        //open and read files
        String inFile = System.getProperty("file.in");
        String outFile = System.getProperty("file.out");
        ArrayList<Object> dataList = new ArrayList<>();

        if (inFile == null || outFile == null) {
            System.out.println("Usage: \n java.exe -jar quicksort-exec -Dfile.in=input.txt -Dfile.out=output.txt");
            outFile = "output.txt";
            //return;
        }
        File outputData = new File(outFile);
        if (outputData.exists()) {
            outputData.delete();
        }
        PrintWriter printWriter = new PrintWriter(outputData);

        BufferedReader inputBuffer = null;
        if (inFile != null) {
            File data = new File(inFile);
            if (!data.exists() && inputFile == null) {
                System.out.println("Error: <-Dfile.in> should exist and contain data");
                return;
            }
            inputBuffer = new BufferedReader(new FileReader(data));
        } else {
            inputBuffer = new BufferedReader(new InputStreamReader(inputFile));

        }

        String inputData = inputBuffer.readLine();
        while (inputData != null) {
            dataList.add(Integer.valueOf(inputData));
            inputData = inputBuffer.readLine();
        }
        inputBuffer.close();
        Icmp icmp = new Icmp();
        QuickSortApp quickSortApp = new QuickSortApp();
        //SORT
        //Object[] v = quickSortApp.sort(dataList.toArray(), 0, dataList.size() - 1, icmp);
        Object[] v = quickSortApp.sort22(dataList.toArray(), 0, dataList.size() - 1, icmp);
        for (Object obj : v) {
            printWriter.println(((Integer) obj).intValue());
            System.out.println(obj);
        }
        printWriter.flush();
        printWriter.close();
    }

    Object[] sort22(Object[] v, int left, int right, Cmp cmp) {
        int l = left, r = right;
        int piv = (int) v[(l + r) / 2];
        while (l <= r) {
            while (cmp.cmp(v[l], piv) < 0 && l <= r) {
                l++;
            }
            while (cmp.cmp(v[r], piv) == 1 && r >= 0) {
                r--;
            }
            if (l <= r) {
                swap(v, l++, r--);
            }
        }
        if (left < r) {
            sort22(v, left, r, cmp);
        }
        if (right > l) {
            sort22(v, l, right, cmp);
        }
        return v;
    }

    void swap(Object[] v, int i, int j) {
        Object temp;
        temp = v[i];
        v[i] = v[j];
        v[j] = temp;
    }

}
