package com.hm;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by W8 on 19.11.2016.
 */
public class ParseVerb extends ParseObject {
    //static public final int PRINT=100, SORTBY=110, THENBY=120;
    static public final String PRINT = "print", SORTBY="sortby", THENBY="thenby";

    private final List<ParseObject> args;
    private boolean isLegal = false;

    public ParseVerb(String s){

        args = new ArrayList<>();
        s = s.toLowerCase();
        value = null;
        type = VERB;
        if(s.equals("print")) {
            value = PRINT;
            isLegal = true;
        }
        if(s.equals("sortby") || "thenby".equals(s)){
            value = SORTBY;
            isLegal = true;
        }
    }

    public ParseVerb getVerb(String s){
        switch (value) {
            case PRINT:
                Print printVerb = new Print(s);
                printVerb.addArgs(getArgs());
                return printVerb;
            case SORTBY:
                return new Sort(s);
        }
        return null;
    }

    public boolean isLegal(){
        return isLegal;
    }

    public void addArgs(ParseObject v){
        args.add(v);
    }

    public void addArgs(List<ParseObject> v){
        args.addAll(v);
    }
    public List<ParseObject> getArgs() {
        return args;
    }
}
