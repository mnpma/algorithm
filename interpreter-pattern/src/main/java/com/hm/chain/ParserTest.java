package com.hm.chain;

import com.hm.Data;

import java.rmi.activation.UnknownObjectException;
import java.util.Calendar;

/**
 * Created by W8 on 26.11.2016.
 */
public class ParserTest {

    public static void main(String... args) throws UnknownObjectException {
        String line = "print lname, frname, club, time sortby club thenby time";
        Parser parser = new Parser(line);
        parser.setData(new Data());
        parser.execute();
    }
}
