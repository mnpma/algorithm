package com.hm.chain;

import com.hm.ParseVerb;
import com.hm.ParseObject;

import java.rmi.activation.UnknownObjectException;
import java.util.LinkedList;

/**
 * Created by W8 on 20.11.2016.
 */
public abstract class InterpChain implements Chain {
    private Chain nextChain;
    protected LinkedList stk;

    public void addChain(Chain c) {
        nextChain = c;
    }

    public abstract boolean interpret() throws UnknownObjectException;

    public Chain getChain() {
        return nextChain;
    }

    public void sendToChain(LinkedList stack) throws UnknownObjectException {
        stk = stack;
        if (!interpret()) {
            nextChain.sendToChain(stk);
        }
    }


    protected void addArgsToVerb() {
        ParseObject v = (ParseObject) stk.pollLast();
        ParseVerb verb = (ParseVerb) stk.pollLast();
        verb.addArgs(v);
        stk.offer(verb);
    }

    protected boolean topStack(int c1, int c2) {

        ParseObject objLast = (ParseObject) stk.pollLast();

        boolean result = (objLast.getType() == c1) &&
                ((ParseObject)stk.peekLast()).getType() == c2;
        stk.offer(objLast);
        return  result;
    }

}
