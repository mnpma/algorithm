package com.hm.chain;

import java.rmi.activation.UnknownObjectException;
import java.util.LinkedList;

/**
 * Created by W8 on 20.11.2016.
 */
public interface Chain {
    void addChain(Chain c);
    void sendToChain(LinkedList stk) throws UnknownObjectException;
    Chain getChain();
}
