package com.hm.chain;

import com.hm.ParseMultVar;
import com.hm.ParseVar;
import com.hm.ParseObject;

import java.util.Arrays;

/**
 * Created by W8 on 26.11.2016.
 */
public class VarVarParse extends InterpChain {

    public boolean interpret()  {
        if(topStack(ParseObject.VAR, ParseObject.VAR)){
            //reduce Var Var to MultVar
            ParseVar v = (ParseVar) stk.pollLast();
            ParseVar v1 = (ParseVar)stk.pollLast();
            ParseMultVar mv= new ParseMultVar(Arrays.asList(v, v1));
            stk.offer(mv);
            return true;
        }
        return false;

    }
}
