package com.hm.chain;

import com.hm.ParseMultVar;
import com.hm.ParseObject;
import com.hm.ParseVar;

import java.util.Arrays;

/**
 * Created by W8 on 27.11.2016.
 */
public class MultVarParse extends InterpChain {

    public boolean interpret() {
        if(topStack( ParseObject.MULTVAR, ParseObject.VAR)){
            //reduce Var Var to MultVar
            ParseMultVar v = (ParseMultVar) stk.pollLast();
            ParseVar v1 = (ParseVar) stk.pollLast();
            ParseMultVar parseMultVar = new ParseMultVar(Arrays.asList(v, v1));

            stk.offer(parseMultVar);
            return true;
        }

        return false;
    }


}
