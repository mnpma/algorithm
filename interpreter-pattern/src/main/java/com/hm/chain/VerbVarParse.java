package com.hm.chain;

import com.hm.*;

import java.rmi.activation.UnknownObjectException;
import java.util.ArrayList;

/**
 * Created by W8 on 26.11.2016.
 */
public class VerbVarParse extends InterpChain {
    private final ArrayList<Verb> actionList;

    public VerbVarParse(ArrayList<Verb> actionList) {
        this.actionList = actionList;
    }

    public boolean interpret() throws UnknownObjectException {
        //!!!
        if (topStack(ParseObject.VAR, ParseObject.VERB)) {
            //reduce Var Verb
            ParseVar v1 = (ParseVar) stk.pollLast();
            ParseVerb v = (ParseVerb) stk.pollLast();

            v.addArgs(v1);
            actionList.add((Verb) v.getVerb(v.getValue()));
            return true;
        }
        return false;
    }
}
