package com.hm.chain;

import com.hm.ParseMultVar;
import com.hm.ParseObject;

import com.hm.ParseVar;

import java.util.Arrays;

/**
 * Created by W8 on 27.11.2016.
 */
public class VarMultVarParse extends InterpChain{
    public boolean interpret() {
        if(topStack(ParseObject.VAR, ParseObject.MULTVAR)){
            //reduce Var Var to MultVar
            ParseVar v = (ParseVar) stk.pollLast();
            ParseMultVar v1 = (ParseMultVar) stk.pollLast();
            ParseMultVar parseMultVar = new ParseMultVar(Arrays.asList(v, v1));

            stk.offer(parseMultVar);
            return true;
        }

        return false;
    }
}
