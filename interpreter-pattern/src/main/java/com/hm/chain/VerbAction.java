package com.hm.chain;

import com.hm.ParseObject;

import com.hm.Verb;

import java.util.ArrayList;

/**
 * Created by W8 on 26.11.2016.
 */
public class VerbAction extends InterpChain {
    private final ArrayList<Verb> actionList;


    public VerbAction(ArrayList<Verb> actionList) {
        this.actionList = actionList;
    }

    public boolean interpret() {
        ParseObject obj = (ParseObject) stk.peekLast();
        if(obj != null && obj instanceof Verb){
            Verb v = (Verb) stk.pollLast();
            actionList.add(v);
            return true;
        }
        return false;
    }
}
