package com.hm.chain;

import com.hm.*;

import java.rmi.activation.UnknownObjectException;
import java.util.ArrayList;

/**
 * Created by W8 on 27.11.2016.
 */
public class VerbMultvarParse extends InterpChain {
    private ArrayList<Verb> actionList;

    public VerbMultvarParse(ArrayList<Verb> actionList) {
        this.actionList = actionList;
    }

    public boolean interpret() throws UnknownObjectException {
        if(topStack( ParseObject.MULTVAR, ParseObject.VERB)){
            //reduce Var Var to MultVar
            ParseMultVar v1 = (ParseMultVar) stk.pollLast();
            ParseVerb v = (ParseVerb) stk.pollLast();

            v.addArgs(v1.getArgs());
            //stk.offer(v);
            actionList.add((Verb) v.getVerb(v.getValue()));

            return true;
        }

        return false;
    }
}
