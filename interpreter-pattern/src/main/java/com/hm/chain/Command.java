package com.hm.chain;

import java.rmi.activation.UnknownObjectException;

/**
 * Created by W8 on 26.11.2016.
 */
public interface Command {
    void execute() throws UnknownObjectException;
}
