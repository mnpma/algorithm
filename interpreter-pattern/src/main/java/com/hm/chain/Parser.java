package com.hm.chain;

import com.hm.*;

import java.rmi.activation.UnknownObjectException;
import java.util.*;

/**
 * Created by W8 on 26.11.2016.
 */
public class Parser implements Command {
    private LinkedList stk;
    private ArrayList<Verb> actionList;
    private Data data;
    private Chain chain;

    public Parser(String line){
        stk = new LinkedList() ;
        actionList = new ArrayList();

        buildStack(line);
        buildChain();
    }
    public void setData(Data data){
        this.data = data;
    }

    private void buildChain(){
        chain = new VarVarParse();

        VarMultVarParse vmvp = new VarMultVarParse();
        chain.addChain(vmvp);
        MultVarParse mvp = new MultVarParse();
        vmvp.addChain(mvp);
        VerbMultvarParse vmp = new VerbMultvarParse(actionList);
        mvp.addChain(vmp);
        VerbVarParse vvp = new VerbVarParse(actionList);
        vmp.addChain(vvp);
/*
        VerbAction va = new VerbAction(actionList);
        vvp.addChain(va);
*/
    }
    private void buildStack(String line){
        String[] tokens = line.split(" ");
        for(String token : tokens){
            ParseObject obj = tokenizer(token);
            if(obj != null){
                stk.offer(obj);
            }
        }
    }

    protected ParseObject tokenizer(String s){
        ParseObject obj = getVerb(s);
        if(obj == null){
            obj = getVar(s);
        }
        return obj;
    }

    protected ParseVerb getVerb(String s){
        ParseVerb v;
        v = new ParseVerb(s);
        if(v.isLegal()){
            return  v; //probably should be added to addArgs
        }
        return  null;
    }

    protected ParseVar getVar(String s){
        ParseVar v;
        v = new ParseVar(s);
        if(v.isLegal()){
            return v;
        }
        return null;
    }
    public void execute() throws UnknownObjectException {
        while(stk.peekLast() != null){
            chain.sendToChain(stk);
        }
        for(Verb v : actionList){
            v.setData(data);
            v.execute();
        }
    }


}
