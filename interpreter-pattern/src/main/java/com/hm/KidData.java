package com.hm;

import java.util.Date;

/**
 * Created by W8 on 20.11.2016.
 */
class KidData {
    private String lname;
    private String frname;
    private String club;
    private Date time;

    public KidData(String lname, String frname, String club, Date time) {
        this.lname = lname;
        this.frname = frname;
        this.club = club;
        this.time = time;
    }

    public String getLname() {
        return lname;
    }

    public String getFrname() {
        return frname;
    }

    public String getClub() {
        return club;
    }

    public Date getTime() {
        return time;
    }

}
