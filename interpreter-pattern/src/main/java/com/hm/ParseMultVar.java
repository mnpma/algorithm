package com.hm;

import java.rmi.activation.UnknownObjectException;
import java.util.List;

/**
 * Created by W8 on 27.11.2016.
 */
public class ParseMultVar extends ParseObject {
    private final List<ParseObject> args;
    public ParseMultVar( List<ParseObject> args) {
        this.args = args;
        this.type = MULTVAR;
    }

    @Override
    public String getValue() throws UnknownObjectException {
        StringBuilder str = new StringBuilder();
        for(ParseObject obj : args){
            str.append(obj.getValue()).append(" ");
        }
        return str.toString();
    }

    public List<ParseObject> getArgs() {
        return args;
    }
}
