package com.hm;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by W8 on 26.11.2016.
 */
public class Data {

    public List<KidData>  dataList = new ArrayList<>();

    {
        for(int i = 0; i< 10 ;i++) {
            dataList.add(new KidData("lname_" + i,
                    "fname_"+i, "club_"+i, Calendar.getInstance().getTime()));
        }
    }

}
