package com.hm;

import java.rmi.activation.UnknownObjectException;

/**
 * Created by W8 on 19.11.2016.
 */
public class ParseObject {
    public static final Integer VERB = 1000, VAR = 1010, MULTVAR = 1020;

    public String getValue() throws UnknownObjectException {
        return value;
    }

    public Integer getType() {
        return type;
    }

    protected String value;
    protected Integer type;


}
