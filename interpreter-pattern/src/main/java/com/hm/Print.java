package com.hm;

import java.rmi.activation.UnknownObjectException;
import java.util.List;

/**
 * Created by W8 on 20.11.2016.
 */
public class Print extends Verb {
    public Print(String s) {
        super(s);
    }

    public void execute() throws UnknownObjectException {
        //output should be organized according to args
        List<ParseObject> args = getArgs();
        for (ParseObject obj : args) {
            System.out.print(obj.getValue().toString() + " ");
        }
        System.out.println();
        for (KidData d : data.dataList) {
            System.out.print(d.getLname() + ",  "
                    + d.getFrname() + ", "
                    + d.getClub() + ", "
                    + d.getTime());
            System.out.println();
        }
    }
}
