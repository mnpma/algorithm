package com.hm;

/**
 * Created by W8 on 20.11.2016.
 */
public class ParseVar extends ParseObject {

    public boolean isLegal(){
        return !("print".equals(value)
                || "sortby".equals(value)
                || "thenby".equals(value));
    }

    public ParseVar(String var) {
        int i = var.indexOf(',');
        this.value = i == -1? var : var.substring(0, var.length() - 1);
        this.type = VAR;
    }

}
