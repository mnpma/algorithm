package com.hm;

import com.hm.chain.Command;

import java.rmi.activation.UnknownObjectException;

/**
 * Created by W8 on 26.11.2016.
 */
public abstract class Verb extends ParseVerb implements Command {
    protected Data data;

    public Verb(String s) {
        super(s);
    }

    public void setData(Data data){
        this.data = data;
    }

    public abstract void execute() throws UnknownObjectException;
}
